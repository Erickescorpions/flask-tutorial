import os

from flask import Flask

# Instancia de Flask en una funcion
# Correr la aplicacion flask --app flaskr run --debug

def create_app(test_config=None):
    # __name__ nombre del modulo actual
    # instance+relative_config le dice a la aplicacion que el archivo de configuracion es relativo a la ruta del programa.
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev', 
        # se establece la ruta del proyecto donde se va a guardar sqlite
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite')
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello
    @app.route('/hello')
    def hello():
        return 'Hello, World!'

    return app