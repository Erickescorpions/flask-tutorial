import sqlite3

import click
from flask import current_app, g


def init_db():
    db = get_db()

    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf8'))

@click.command('init.db')
def init_db_command():
    init_db()
    click.echo('Inicializando la base de datos')

# cada que exista una peticion va a obtener la instancia de la base 
# de datos, en caso de que no exista la va a crear.
def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            # current_app es una funcion que nos permite 
            # gestionar las peticiones
            current_app.config['DATABASE'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )

        g.db.row_factory = sqlite3.Row

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()

# g es un obketo unco para cada peticion. Se utiliza para guardar informacion
# accedida en varias funciones en una peticion. 